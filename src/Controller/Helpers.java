package Controller;

import java.util.Scanner;

/**
 *
 * @author anhnb
 */
public class Helpers {

    Scanner in;

    public Helpers() {
        in = new Scanner(System.in);
    }

    public int checkInt() {
        String n = "";
        while (true) {
            try {
                n = in.nextLine();
                return Integer.parseInt(n);
            } catch (Exception e) {
                System.out.println("Enter again");
            }
        }

    }

}
